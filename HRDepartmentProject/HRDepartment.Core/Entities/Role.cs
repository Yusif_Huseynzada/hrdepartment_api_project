﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRDepartment.Core.Entities
{
    public class Role: BaseEntity
    {
        public string Name { get; set; }
        public List<AppUser> AppUsers { get; set;}
        public void SetDetail(string name)
        {
            this.Name = name;
        }
    }
}