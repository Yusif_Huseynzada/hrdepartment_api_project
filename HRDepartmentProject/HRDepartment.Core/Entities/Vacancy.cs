﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRDepartment.Core.Entities
{
    public class Vacancy:BaseEntity
    {
        public int CategoryId { get; set; }
        public string Name { get; set; }
        public string WorkCommitment { get; set; }
        public string Requirement { get; set; }
        public decimal? Salary { get; set; }
        public DateTime CreatedAt { get; set; } = DateTime.UtcNow.AddHours(4);
        public DateTime Deadline { get; set; } = DateTime.UtcNow.AddDays(30);
        public Category Category { get; set; } 
       
    }
}
