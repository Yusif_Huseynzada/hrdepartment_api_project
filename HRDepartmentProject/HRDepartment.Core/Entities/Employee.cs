﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRDepartment.Core.Entities
{
    public class Employee : BaseEntity
    {
        public int CategoryId { get; set; }
        public string FullName { get; set; }
        public string Email { get; set; }
        public string DateOfRecruitment { get; set; }
        public int Age { get; set; }
        public decimal Salary { get; set; }
        public string Image { get; set; }
        public Category Category { get; set; }
    }
}
