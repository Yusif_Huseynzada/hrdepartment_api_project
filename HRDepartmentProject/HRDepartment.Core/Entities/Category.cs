﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRDepartment.Core.Entities
{
    public class Category:BaseEntity
    {
        public string Name { get; set; }
        public List<Vacancy> Vacancies { get; set; }
        public List<Employee> Employees { get; set; }

        public void SetDetail(string name)
        {
            this.Name = name;
        }
    }
}
