﻿using HRDepartment.Api.Admin.Dtos.EmployeeDtos;
using Microsoft.AspNetCore.Mvc;

namespace HRDepartment.Api.Admin.Services.Interfaces
{
    public interface IEmployeeService
    {
        Task<IActionResult> Create(EmployeePostDto postDto);
        Task<IActionResult> Edit(EmployeePutDto putDto);
        Task<IActionResult> GetAll(int page);
        Task<IActionResult> Get(int id);
        Task<IActionResult> Delete(int id);
    }
}
