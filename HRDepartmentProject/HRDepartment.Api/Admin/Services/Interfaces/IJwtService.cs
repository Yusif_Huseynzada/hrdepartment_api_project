﻿using HRDepartment.Api.Admin.Dtos.AccountDtos;
using HRDepartment.Core.Entities;
using Microsoft.AspNetCore.Mvc;

namespace HRDepartment.Api.Services
{
    public interface IJwtService
    {
        string GenerateToken(AppUser user, IConfiguration config);
    }
}
