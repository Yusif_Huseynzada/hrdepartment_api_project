﻿using HRDepartment.Api.Admin.Dtos.VacancyDtos;
using Microsoft.AspNetCore.Mvc;

namespace HRDepartment.Api.Admin.Services.Interfaces
{
    public interface IVacancyService
    {
        Task<IActionResult> Create(VacancyPostDto postDto);
        Task<IActionResult> Edit(VacancyPutDto putDto);
        Task<IActionResult> GetAll(int page);
        Task<IActionResult> Get(int id);
        Task<IActionResult> Delete(int id);
    }
}
