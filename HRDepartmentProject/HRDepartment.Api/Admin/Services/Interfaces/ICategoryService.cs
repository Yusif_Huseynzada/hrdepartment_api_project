﻿using HRDepartment.Api.Admin.Dtos.CategoryDtos;
using Microsoft.AspNetCore.Mvc;

namespace HRDepartment.Api.Admin.Services.Interfaces
{
    public interface ICategoryService
    {
        Task<IActionResult> Create(CategoryPostDto postDto);
        Task<IActionResult> Edit(CategoryPutDto putDto);
        Task<IActionResult> GetAll(int page);
        Task<IActionResult> Get(int id);
        Task<IActionResult> Delete(int id);
    }
}
