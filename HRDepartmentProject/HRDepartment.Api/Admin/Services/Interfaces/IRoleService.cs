﻿using HRDepartment.Api.Admin.Dtos.RoleDtos;
using Microsoft.AspNetCore.Mvc;

namespace HRDepartment.Api.Admin.Services.Interfaces
{
    public interface IRoleService
    {
        Task<IActionResult> Create(RolePostDto postDto);
        Task<IActionResult> Edit(RolePutDto putDto);
        Task<IActionResult> GetAll(int page);
        Task<IActionResult> Get(int id);
        Task<IActionResult> Delete(int id);
    }
}
