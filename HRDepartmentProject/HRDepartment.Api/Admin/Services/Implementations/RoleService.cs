﻿using AutoMapper;
using HRDepartment.Api.Admin.Dtos.CategoryDtos;
using HRDepartment.Api.Admin.Dtos;
using HRDepartment.Api.Admin.Services.Interfaces;
using HRDepartment.Core.Entities;
using HRDepartment.Core.Repositories;
using HRDepartment.Data.Repositories;
using Microsoft.AspNetCore.Mvc;
using HRDepartment.Api.Admin.Dtos.RoleDtos;

namespace HRDepartment.Api.Admin.Services.Implementations
{
    public class RoleService : IRoleService
    {
        private readonly IMapper _mapper;
        private readonly IRoleRepository _roleRepository;

        public RoleService(IMapper mapper, IRoleRepository roleRepository)
        {
            _mapper = mapper;
            _roleRepository = roleRepository;
        }

        public async Task<IActionResult> Create(RolePostDto postDto)
        {
            if (await _roleRepository.IsExistAsync(x => x.Name == postDto.Name))
            return new BadRequestObjectResult(new { error = new { field = "Name", message = "Role already created!" } });

            Role role = _mapper.Map<Role>(postDto);

            await _roleRepository.AddAsync(role);
            await _roleRepository.CommitAsync();

            return new StatusCodeResult(201);
        }

        public async Task<IActionResult> Edit(RolePutDto putDto)
        {
            Role role = await _roleRepository.GetAsync(x => x.Id == putDto.Id);
            if (role == null) return new NotFoundResult();

            role.SetDetail(putDto.Name);
            await _roleRepository.UpdateAsync(role);
            return new NoContentResult();
        }

        public async Task<IActionResult> GetAll(int page)
        {
            var query = _roleRepository.GetAll(x => true);
            var roleDtos = _mapper.Map<List<RoleListItemDto>>(query.Skip((page - 1) * 4).Take(4));

            PaginationListDto<RoleListItemDto> model =
            new PaginationListDto<RoleListItemDto>(roleDtos, page, 4, query.Count());
            return new OkObjectResult(model);
        }

        public async Task<IActionResult> Get(int id)
        {
            Role role = await _roleRepository.GetAsync(x => x.Id == id);
            if (role == null)
            return new NotFoundResult();

            RoleGetDto roleDto = _mapper.Map<RoleGetDto>(role);
            return new OkObjectResult(roleDto);
        }

        public async Task<IActionResult> Delete(int id)
        {
            Role role = await _roleRepository.GetAsync(x => x.Id == id);
            if (role == null)
            return new NotFoundResult();

            _roleRepository.Remove(role);
            _roleRepository.Commit();
            return new NoContentResult();
        }
    }
}
