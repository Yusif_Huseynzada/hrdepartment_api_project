﻿using AutoMapper;
using HRDepartment.Api.Admin.Dtos.RoleDtos;
using HRDepartment.Api.Admin.Dtos;
using HRDepartment.Api.Admin.Services.Interfaces;
using HRDepartment.Core.Entities;
using HRDepartment.Core.Repositories;
using HRDepartment.Data.Repositories;
using Microsoft.AspNetCore.Mvc;
using HRDepartment.Api.Admin.Dtos.VacancyDtos;
using System.Data;

namespace HRDepartment.Api.Admin.Services.Implementations
{
    public class VacancyService : IVacancyService
    {
        private readonly IMapper _mapper;
        private readonly IVacancyRepository _vacancyRepository;
        private readonly ICategoryRepository _categoryRepository;


        public VacancyService(IMapper mapper, IVacancyRepository vacancyRepository, ICategoryRepository categoryRepository)
        {
            _mapper = mapper;
            _vacancyRepository = vacancyRepository;
            _categoryRepository = categoryRepository;
        }
        public async Task<IActionResult> Create(VacancyPostDto postDto)
        {
            if (await _vacancyRepository.IsExistAsync(x => x.Name == postDto.Name))
            return new BadRequestObjectResult(new { error = new { field = "Name", message = "Vacancy already exist!" } });

            Vacancy vacancy = _mapper.Map<Vacancy>(postDto);

            await _vacancyRepository.AddAsync(vacancy);
            await _vacancyRepository.CommitAsync();

            return new ObjectResult(vacancy) { StatusCode = 201 };
        }

        public async Task<IActionResult> Edit(VacancyPutDto putDto)
        {
            Vacancy vacancy = await _vacancyRepository.GetAsync(x => x.Id == putDto.Id);

            if (vacancy == null) return new NotFoundResult();

            _mapper.Map(putDto, vacancy);

            await _vacancyRepository.UpdateAsync(vacancy);
            return new NoContentResult();
        }
        public async Task<IActionResult> GetAll(int page)
        {
            var query = _vacancyRepository.GetAll(x => true);
            var vacancyDtos = _mapper.Map<List<VacancyListItemDto>>(query.Skip((page - 1) * 4).Take(4));

            PaginationListDto<VacancyListItemDto> model =
                new PaginationListDto<VacancyListItemDto>(vacancyDtos, page, 4, query.Count());
            return new OkObjectResult(model);
        }

        public async Task<IActionResult> Get(int id)
        {
            Vacancy vacancy = await _vacancyRepository.GetAsync(x => x.Id == id, "Category");
            if (vacancy == null)
                return new NotFoundResult();

            VacancyGetDto vacancyDto = _mapper.Map<VacancyGetDto>(vacancy);
            return new OkObjectResult(vacancyDto);
        }

        public async Task<IActionResult> Delete(int id)
        {
            Vacancy vacancy = await _vacancyRepository.GetAsync(x => x.Id == id);
            if (vacancy == null)
                return new NotFoundResult();

            _vacancyRepository.Remove(vacancy);
            _vacancyRepository.Commit();
            return new NoContentResult();
        }
    }
}
