﻿using AutoMapper;
using HRDepartment.Api.Admin.Dtos.AccountDtos;
using HRDepartment.Api.Admin.Dtos;
using HRDepartment.Api.Admin.Services.Interfaces;
using HRDepartment.Core.Entities;
using HRDepartment.Core.Repositories;
using HRDepartment.Data.Repositories;
using Microsoft.AspNetCore.Mvc;
using HRDepartment.Api.Admin.Dtos.CategoryDtos;
using System.Data;

namespace HRDepartment.Api.Admin.Services.Implementations
{
    public class CategoryService : ICategoryService
    {
        private readonly IMapper _mapper;
        private readonly ICategoryRepository _categoryRepository;

        public CategoryService(IMapper mapper, ICategoryRepository categoryRepository)
        {
            _mapper = mapper;
            _categoryRepository = categoryRepository;
        }

        public async Task<IActionResult> Create(CategoryPostDto postDto)
        {
            if (await _categoryRepository.IsExistAsync(x => x.Name == postDto.Name))
            return new BadRequestObjectResult(new { error = new { field = "Name", message = "Category already created!" } });

            Category category = _mapper.Map<Category>(postDto);

            await _categoryRepository.AddAsync(category);
            await _categoryRepository.CommitAsync();

            return new StatusCodeResult(201);
        }

        public async Task<IActionResult> Edit(CategoryPutDto putDto)
        {
            Category category = await _categoryRepository.GetAsync(x => x.Id == putDto.Id);

            if (category == null) return new NotFoundResult();
            if (await _categoryRepository.IsExistAsync(x => x.Id != putDto.Id && x.Name == putDto.Name)) 
            return new BadRequestObjectResult(new { error = "Bad Request" });

            category.SetDetail(putDto.Name);

            await _categoryRepository.UpdateAsync(category);
            return new NoContentResult();
        }

        public async Task<IActionResult> GetAll(int page)
        {
            var query = _categoryRepository.GetAll(x => true);
            var categoryDtos = _mapper.Map<List<CategoryListItemDto>>(query.Skip((page - 1) * 4).Take(4));

            PaginationListDto<CategoryListItemDto> model =
            new PaginationListDto<CategoryListItemDto>(categoryDtos, page, 4, query.Count());
            return new OkObjectResult(model);
        }

        public async Task<IActionResult> Get(int id)
        {
            Category category = await _categoryRepository.GetAsync(x => x.Id == id);
            if (category == null)
                return new NotFoundResult();

            CategoryGetDto categoryDto = _mapper.Map<CategoryGetDto>(category);
            return new OkObjectResult(categoryDto);
        }

        public async Task<IActionResult> Delete(int id)
        {
            Category category = await _categoryRepository.GetAsync(x => x.Id == id);
            if (category == null)
                return new NotFoundResult();

            _categoryRepository.Remove(category);
            _categoryRepository.Commit();
            return new NoContentResult();
        }
    }
}
