﻿using AutoMapper;
using HRDepartment.Api.Admin.Dtos.RoleDtos;
using HRDepartment.Api.Admin.Dtos;
using HRDepartment.Api.Admin.Services.Interfaces;
using HRDepartment.Core.Entities;
using HRDepartment.Core.Repositories;
using HRDepartment.Data.Repositories;
using Microsoft.AspNetCore.Mvc;
using HRDepartment.Api.Admin.Dtos.EmployeeDtos;
using HRDepartment.Api.Helpers;
using HRDepartment.Api.Admin.Dtos.VacancyDtos;

namespace HRDepartment.Api.Admin.Services.Implementations
{
    public class EmployeeService : IEmployeeService
    {
        private readonly IMapper _mapper;
        private readonly IEmployeeRepository _employeeRepository;
        private readonly ICategoryRepository _categoryRepository;
        private readonly IWebHostEnvironment _env;

        public EmployeeService(IMapper mapper, IEmployeeRepository employeeRepository, ICategoryRepository categoryRepository, IWebHostEnvironment env)
        {
            _mapper = mapper;
            _employeeRepository = employeeRepository;
            _categoryRepository = categoryRepository;
            _env = env;
        }

        public async Task<IActionResult> Create(EmployeePostDto postDto)
        {
            Employee employee = _mapper.Map<Employee>(postDto);
            employee.Image = FileManager.Save(postDto.ImageFile, _env.WebRootPath, "uploads/employees");

            await _employeeRepository.AddAsync(employee);
            await _employeeRepository.CommitAsync();

            return new ObjectResult(employee) { StatusCode = 201 };
        }

        public async Task<IActionResult> Edit(EmployeePutDto putDto)
        {
            Employee employee = await _employeeRepository.GetAsync(x => x.Id == putDto.Id);

            if (employee == null) return new NotFoundResult();

            _mapper.Map(putDto, employee);

            await _employeeRepository.UpdateAsync(employee);
            return new NoContentResult();
        }
        public async Task<IActionResult> GetAll(int page)
        {
            var query = _employeeRepository.GetAll(x => true);
            var employeeDtos = _mapper.Map<List<EmployeeListItemDto>>(query.Skip((page - 1) * 4).Take(4));

            PaginationListDto<EmployeeListItemDto> model =
            new PaginationListDto<EmployeeListItemDto>(employeeDtos, page, 4, query.Count());
            return new OkObjectResult(model);
        }

        public async Task<IActionResult> Get(int id)
        {
            Employee employee = await _employeeRepository.GetAsync(x => x.Id == id, "Category");
            if (employee == null)
                return new NotFoundResult();

            EmployeeGetDto employeeDto = _mapper.Map<EmployeeGetDto>(employee);
            return new OkObjectResult(employeeDto);
        }

        public async Task<IActionResult> Delete(int id)
        {
            Employee employee = await _employeeRepository.GetAsync(x => x.Id == id);
            if (employee == null)
                return new NotFoundResult();

            _employeeRepository.Remove(employee);
            _employeeRepository.Commit();
            return new NoContentResult();
        }
    }
}
