﻿using AutoMapper;
using HRDepartment.Api.Admin.Dtos.AccountDtos;
using HRDepartment.Api.Admin.Dtos.CategoryDtos;
using HRDepartment.Api.Admin.Dtos.EmployeeDtos;
using HRDepartment.Api.Admin.Dtos.RoleDtos;
using HRDepartment.Api.Admin.Dtos.VacancyDtos;
using HRDepartment.Core.Entities;
using StoreProjectAPI.Helpers;

namespace HRDepartment.Api.Admin.Profiles
{
    public class AdminMapper:Profile
    {
        private readonly IHttpContextAccessor _httpAccessor;

        public AdminMapper(IHttpContextAccessor httpAccessor)
        {
            _httpAccessor = httpAccessor;

            CreateMap<Category, CategoryGetDto>();
            CreateMap<CategoryPostDto, Category>();
            CreateMap<CategoryPutDto, Category>();
            CreateMap<Category, CategoryListItemDto>();
            CreateMap<Category, CategoryInEmployeeGetDto>();
            CreateMap<Category, CategoryInVacancyGetDto>();
           

            CreateMap<Role, RoleInAppUserGetDto>();
            CreateMap<Role, RoleListItemDto>();
            CreateMap<Role, RoleGetDto>();
            CreateMap<RolePostDto, Role>();
            CreateMap<RolePutDto, Role>();


            CreateMap<EmployeePostDto, Employee>();
            CreateMap<EmployeePutDto, Employee>();
            CreateMap<Employee, EmployeeGetDto>()
                .ForMember(x => x.ImageURL, f => f.MapFrom(s => $"{_httpAccessor.HttpContext.Request.BaseUrl()}/uploads/employees/{s.Image}"));
            CreateMap<Employee, EmployeeListItemDto>();

            CreateMap<VacancyPostDto, Vacancy>();
            CreateMap<VacancyPutDto, Vacancy>();
            CreateMap<Vacancy, VacancyGetDto>();
            CreateMap<Vacancy, VacancyListItemDto>();

            CreateMap<AppUserPostDto, AppUser>();
            CreateMap<AppUserPutDto, AppUser>();
            CreateMap<AppUser, AppUserGetDto>();
            CreateMap<AppUser, AppUserListItemDto>();
        }

    }
}
