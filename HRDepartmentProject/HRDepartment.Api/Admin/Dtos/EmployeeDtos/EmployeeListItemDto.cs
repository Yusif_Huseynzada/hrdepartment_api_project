﻿namespace HRDepartment.Api.Admin.Dtos.EmployeeDtos
{
    public class EmployeeListItemDto
    {
        public int Id { get; set; }
        public int CategoryId { get; set; }
        public string FullName { get; set; }
        public string Email { get; set; }
        public string DateOfRecruitment { get; set; }
        public int Age { get; set; }
        public decimal Salary { get; set; }
    }
}
