﻿using FluentValidation;

namespace HRDepartment.Api.Admin.Dtos.EmployeeDtos
{
    public class EmployeePostDto
    {
        public int CategoryId { get; set; }
        public string FullName { get; set; }
        public int Age { get; set; }
        public decimal Salary { get; set; }
        public string Email { get; set; }
        public string DateOfRecruitment { get; set; }
        public IFormFile ImageFile { get; set; }
           
    }
    public class EmployeePostDtoValidator : AbstractValidator<EmployeePostDto>
    {
        public EmployeePostDtoValidator()
        {
            RuleFor(x => x.FullName).NotNull().NotEmpty().MaximumLength(50);
            RuleFor(x => x.Email).NotNull().NotEmpty().MaximumLength(50);
            RuleFor(x => x.DateOfRecruitment).NotNull().NotEmpty().MaximumLength(25);
            RuleFor(x => x.Age).GreaterThanOrEqualTo(0).LessThanOrEqualTo(65);
            RuleFor(x => x.Salary).GreaterThanOrEqualTo(0);

            RuleFor(x => x).Custom((x, context) =>
            {
                if (x.ImageFile == null)
                {
                    context.AddFailure("ImageFile", "ImageFile is required");
                }
                else if (x.ImageFile.ContentType != "image/png" && x.ImageFile.ContentType != "image/jpeg")
                {
                    context.AddFailure("ImageFile", "File type must be png,jpg or jpeg");
                }
                else if (x.ImageFile.Length > 2097152)
                {
                    context.AddFailure("ImageFile", "File size must be less or equal than 2MB");
                }
            });
        }

    }
}
