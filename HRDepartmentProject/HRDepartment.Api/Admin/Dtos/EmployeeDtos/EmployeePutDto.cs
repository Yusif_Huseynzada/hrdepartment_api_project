﻿using FluentValidation;
using HRDepartment.Api.Admin.Dtos.VacancyDtos;
using Microsoft.OpenApi.Any;
using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace HRDepartment.Api.Admin.Dtos.EmployeeDtos
{
    public class EmployeePutDto
    {
        public int Id { get; set; }
        public int CategoryId { get; set; }
        public string FullName { get; set; }
        public string Email { get; set; }
        public string DateOfRecruitment { get; set; }
        public int Age { get; set; }
        public decimal Salary { get; set; }
        public IFormFile? ImageFile { get; set; }
    }
    public class EmployeePutDtoValidator : AbstractValidator<EmployeePutDto>
    {
        public EmployeePutDtoValidator()
        {
            RuleFor(x => x.FullName).NotNull().NotEmpty().MaximumLength(50);
            RuleFor(x => x.Email).NotNull().NotEmpty().MaximumLength(50);
            RuleFor(x => x.DateOfRecruitment).NotNull().NotEmpty().MaximumLength(25);
            RuleFor(x => x.Age).GreaterThanOrEqualTo(0).LessThanOrEqualTo(65);
            RuleFor(x => x.Salary).NotNull().GreaterThanOrEqualTo(0);
            RuleFor(x => x.CategoryId).NotNull();
         

            RuleFor(x => x.ImageFile)
                .Must(x => x == null || x.ContentType == "image/png" || x.ContentType == "image/jpeg").WithMessage("File type must be png,jpg or jpeg")
                .Must(x => x == null || x.Length <= 2097152).WithMessage("File size must be less or equal than 2MB");
        }
    }
    public class EmployeePutDtoSchemaFilter : ISchemaFilter
    {
        public void Apply(OpenApiSchema schema, SchemaFilterContext context)
        {
            if (context.Type == typeof(EmployeePutDto))
            {
                schema.Example = new OpenApiObject
                {
                    ["categoryId"] = new OpenApiInteger(1),
                    ["fullName"] = new OpenApiString("John Doe"),
                    ["email"] = new OpenApiString("johndoe@example.com"),
                    ["dateOfRecruitment"] = new OpenApiString("2023-10-30"),
                    ["age"] = new OpenApiInteger(30),
                    ["salary"] = new OpenApiDouble(50000.50),
                };
            }
        }
    }
}
