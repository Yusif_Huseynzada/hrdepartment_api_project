﻿using HRDepartment.Core.Entities;

namespace HRDepartment.Api.Admin.Dtos.EmployeeDtos
{
    public class EmployeeGetDto
    {
        public int Id { get; set; }
        public string FullName { get; set; }
        public int Age { get; set; }
        public decimal Salary { get; set; }
        public string Email { get; set; }
        public string DateOfRecruitment { get; set; }
        public string ImageURL { get; set; }
        public CategoryInEmployeeGetDto Category { get; set; }
    }
    public class CategoryInEmployeeGetDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
