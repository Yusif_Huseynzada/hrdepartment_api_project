﻿using FluentValidation;
using HRDepartment.Api.Admin.Dtos.VacancyDtos;

namespace HRDepartment.Api.Admin.Dtos.AccountDtos
{
    public class AppUserPostDto
    {
        public int RoleId { get; set; }
        public string FullName { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
    }

    public class AppUserPostDtoValidator : AbstractValidator<AppUserPostDto>
    {
        public AppUserPostDtoValidator()
        {
            RuleFor(x => x.FullName).NotNull().NotEmpty().MaximumLength(50);
            RuleFor(x => x.UserName).NotNull().NotEmpty().MaximumLength(20).MinimumLength(6);
            RuleFor(x => x.Password).NotNull().NotEmpty().MaximumLength(100).MinimumLength(6);
            RuleFor(x => x.Email).NotNull().NotEmpty().MaximumLength(50);
          
        }

    }
}
