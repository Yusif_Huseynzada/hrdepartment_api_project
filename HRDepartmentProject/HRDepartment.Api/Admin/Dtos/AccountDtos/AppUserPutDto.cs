﻿using FluentValidation;

namespace HRDepartment.Api.Admin.Dtos.AccountDtos
{
    public class AppUserPutDto
    {
        public int Id { get; set; }
        public int RoleId { get; set; }
        public string FullName { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
    }

    public class AppUserPutDtoValidator : AbstractValidator<AppUserPutDto>
    {
        public AppUserPutDtoValidator()
        {
            RuleFor(x => x.FullName).NotNull().NotEmpty().MaximumLength(50);
            RuleFor(x => x.UserName).NotNull().NotEmpty().MaximumLength(20).MinimumLength(6);
            RuleFor(x => x.Password).NotNull().NotEmpty().MaximumLength(100).MinimumLength(6);
            RuleFor(x => x.Email).NotNull().NotEmpty().MaximumLength(500);
            RuleFor(x => x.RoleId).NotNull().WithMessage("RoleId cannot be null or empty");
        }

    }
}

