﻿using FluentValidation;
using Microsoft.OpenApi.Any;
using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace HRDepartment.Api.Admin.Dtos.VacancyDtos
{
    public class VacancyPutDto
    {
        public int Id { get; set; }
        public int CategoryId { get; set; }
        public string Name { get; set; }
        public string WorkCommitment { get; set; }
        public string Requirement { get; set; }
        public decimal? Salary { get; set; }
    }

    public class VacancyPutDtoValidator : AbstractValidator<VacancyPutDto>
    {
        public VacancyPutDtoValidator()
        {
            RuleFor(x => x.Name).NotNull().NotEmpty().MaximumLength(50);
            RuleFor(x => x.WorkCommitment).NotNull().NotEmpty().MaximumLength(500);
            RuleFor(x => x.Requirement).NotNull().NotEmpty().MaximumLength(500);
            RuleFor(x => x.Salary).GreaterThanOrEqualTo(0).LessThanOrEqualTo(15000);
            RuleFor(x => x.CategoryId).NotNull();
        }

    }
    public class VacancyPutDtoSchemaFilter : ISchemaFilter
    {
        public void Apply(OpenApiSchema schema, SchemaFilterContext context)
        {
            if (context.Type == typeof(VacancyPutDto))
            {
                schema.Example = new OpenApiObject
                {
                    ["categoryId"] = new OpenApiInteger(1),
                    ["name"] = new OpenApiString("ExampleName"),
                    ["workCommitment"] = new OpenApiString("ExampleWorkCommitment"),
                    ["requirement"] = new OpenApiString("ExampleRequirment"),
                    ["salary"] = new OpenApiDouble(1000),
                };
            }
        }
    }
}
