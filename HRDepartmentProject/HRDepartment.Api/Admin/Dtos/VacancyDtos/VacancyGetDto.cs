﻿using HRDepartment.Api.Admin.Dtos.EmployeeDtos;

namespace HRDepartment.Api.Admin.Dtos.VacancyDtos
{
    public class VacancyGetDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string WorkCommitment { get; set; }
        public string Requirement { get; set; }
        public DateTime CreatedAt { get; set; } = DateTime.UtcNow.AddHours(4);
        public DateTime Deadline { get; set; } = DateTime.UtcNow.AddDays(30);
        public CategoryInVacancyGetDto Category { get; set; }
    }
    public class CategoryInVacancyGetDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
