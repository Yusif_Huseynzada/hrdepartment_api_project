﻿namespace HRDepartment.Api.Admin.Dtos.VacancyDtos
{
    public class VacancyListItemDto
    {
        public int Id { get; set; }
        public int CategoryId { get; set; }
        public string Name { get; set; }
        public string WorkCommitment { get; set; }
        public string Requirement { get; set; }
        public DateTime CreatedAt { get; set; } = DateTime.UtcNow.AddHours(4);
        public DateTime Deadline { get; set; } = DateTime.UtcNow.AddDays(30);
    }
}
