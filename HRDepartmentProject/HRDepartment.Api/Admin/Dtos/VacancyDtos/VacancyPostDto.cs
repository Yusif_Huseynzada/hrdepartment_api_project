﻿using FluentValidation;
using HRDepartment.Api.Admin.Dtos.EmployeeDtos;

namespace HRDepartment.Api.Admin.Dtos.VacancyDtos
{
    public class VacancyPostDto
    {
        public int CategoryId { get; set; }
        public string Name { get; set; }
        public string WorkCommitment { get; set; }
        public string Requirement { get; set; }
        public decimal? Salary { get; set; }
        
    }

    public class VacancyPostDtoValidator : AbstractValidator<VacancyPostDto>
    {
        public VacancyPostDtoValidator()
        {
            RuleFor(x => x.Name).NotNull().NotEmpty().MaximumLength(50);
            RuleFor(x => x.WorkCommitment).NotNull().NotEmpty().MaximumLength(500);
            RuleFor(x => x.Requirement).NotNull().NotEmpty().MaximumLength(500);
            RuleFor(x => x.Salary).GreaterThanOrEqualTo(0).LessThanOrEqualTo(15000);
        }

    }
}
