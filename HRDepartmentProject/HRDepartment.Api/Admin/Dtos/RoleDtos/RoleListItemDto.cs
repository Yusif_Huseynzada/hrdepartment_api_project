﻿namespace HRDepartment.Api.Admin.Dtos.RoleDtos
{
    public class RoleListItemDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
