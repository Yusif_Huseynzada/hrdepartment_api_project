﻿using FluentValidation;
using HRDepartment.Api.Admin.Dtos.CategoryDtos;

namespace HRDepartment.Api.Admin.Dtos.RoleDtos
{
    public class RolePutDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
    public class RolePutDtoValidator : AbstractValidator<RolePutDto>
    {
        public RolePutDtoValidator()
        {
            RuleFor(x => x.Name).MaximumLength(30).NotEmpty();
        }
    }
}
