﻿namespace HRDepartment.Api.Admin.Dtos.RoleDtos
{
    public class RoleGetDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
