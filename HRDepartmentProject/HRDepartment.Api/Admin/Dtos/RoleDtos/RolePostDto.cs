﻿using FluentValidation;
using HRDepartment.Api.Admin.Dtos.CategoryDtos;

namespace HRDepartment.Api.Admin.Dtos.RoleDtos
{
    public class RolePostDto
    {
        public string Name { get; set; }
    }
    public class RolePostDtoValidator : AbstractValidator<RolePostDto>
    {
        public RolePostDtoValidator()
        {
            RuleFor(x => x.Name).MaximumLength(30).NotEmpty();
        }
    }
}
