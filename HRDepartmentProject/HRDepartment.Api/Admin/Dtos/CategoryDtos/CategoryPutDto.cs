﻿using FluentValidation;

namespace HRDepartment.Api.Admin.Dtos.CategoryDtos
{
    public class CategoryPutDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
    public class CategoryPutDtoValidator : AbstractValidator<CategoryPutDto>
    {
        public CategoryPutDtoValidator()
        {
            RuleFor(x => x.Name).MaximumLength(30).NotEmpty();
        }
    }
}
