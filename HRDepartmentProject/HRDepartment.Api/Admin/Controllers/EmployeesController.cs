﻿using AutoMapper;
using HRDepartment.Api.Admin.Dtos.CategoryDtos;
using HRDepartment.Api.Admin.Dtos;
using HRDepartment.Core.Entities;
using HRDepartment.Core.Repositories;
using HRDepartment.Data.Repositories;
using Microsoft.AspNetCore.Mvc;
using HRDepartment.Api.Admin.Dtos.EmployeeDtos;
using HRDepartment.Api.Helpers;
using HRDepartment.Api.Admin.Services.Interfaces;
using HRDepartment.Api.Admin.Services.Implementations;
using Microsoft.AspNetCore.Mvc.RazorPages;
using System.Data;
using Microsoft.AspNetCore.Authorization;

namespace HRDepartment.Api.Admin.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EmployeesController: ControllerBase
    {
       
        private readonly IEmployeeService _employeeService;


        public EmployeesController(IEmployeeService employeeService)
        {
            _employeeService = employeeService;
        }

        [Authorize(Roles = "Menecer")]
        [HttpPost("")]
        public async Task<IActionResult> Create([FromForm] EmployeePostDto postDto)
        {
            return await _employeeService.Create(postDto);
        }

        [Authorize(Roles = "Menecer")]
        [HttpPut]
        public async Task<IActionResult> Edit([FromForm] EmployeePutDto putDto)
        {
            return await _employeeService.Edit(putDto);
        }

        [Authorize(Roles = "Insan Resurslari, Menecer, Isci")]
        [HttpGet("all")]
        public async Task<IActionResult> GetAll(int page = 1)
        {
            return await _employeeService.GetAll(page);
        }

        [Authorize(Roles = "Insan Resurslari, Menecer, Isci")]
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            return await _employeeService.Get(id);

        }

        [Authorize(Roles = "Insan Resurslari, Menecer")]
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            return await _employeeService.Delete(id);
        }
    }
}
