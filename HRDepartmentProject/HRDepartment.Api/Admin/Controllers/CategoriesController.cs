﻿using AutoMapper;
using HRDepartment.Api.Admin.Dtos;
using HRDepartment.Api.Admin.Dtos.CategoryDtos;
using HRDepartment.Api.Admin.Dtos.EmployeeDtos;
using HRDepartment.Api.Admin.Services.Interfaces;
using HRDepartment.Core.Entities;
using HRDepartment.Core.Repositories;
using HRDepartment.Data.Repositories;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using System.Data;

namespace HRDepartment.Api.Admin.Controllers
{
   
    [Route("api/[controller]")]
    [ApiController]
    public class CategoriesController:ControllerBase
    {
        private readonly ICategoryService _categoryService;

        public CategoriesController(ICategoryService categoryService)
        {
            _categoryService = categoryService;
        }

        [Authorize(Roles = "Menecer")]
        [HttpPost("")]
        public async Task<IActionResult> Create(CategoryPostDto postDto)
        {
            return await _categoryService.Create(postDto);

        }

        [Authorize(Roles = "Menecer")]
        [HttpPut]
        public async Task<IActionResult> Edit(CategoryPutDto putDto)
        {
            return await _categoryService.Edit(putDto);

        }

        [Authorize(Roles = "Insan Resurslari, Menecer, Isci")]
        [HttpGet("all")]
        public async Task<IActionResult> GetAll(int page = 1)
        {
            return await _categoryService.GetAll(page);
        }

        [Authorize(Roles = "Insan Resurslari, Menecer, Isci")]
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            return await _categoryService.Get(id);

        }

        [Authorize(Roles = "Insan Resurslari, Menecer")]
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            return await _categoryService.Delete(id);
        }

    }
}
