﻿using AutoMapper;
using HRDepartment.Api.Admin.Dtos.CategoryDtos;
using HRDepartment.Api.Admin.Dtos;
using HRDepartment.Core.Entities;
using HRDepartment.Core.Repositories;
using Microsoft.AspNetCore.Mvc;
using HRDepartment.Api.Admin.Dtos.RoleDtos;
using System.Data;
using HRDepartment.Api.Admin.Services.Interfaces;
using HRDepartment.Api.Admin.Services.Implementations;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Authorization;

namespace HRDepartment.Api.Admin.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RolesController: ControllerBase
    {
        private readonly IRoleService _roleService;

        public RolesController(IRoleService roleService)
        {
            _roleService = roleService;
        }

        [Authorize(Roles = "Insan Resurslari")]
        [HttpPost("")]
        public async Task<IActionResult> Create(RolePostDto postDto)
        {
            return await _roleService.Create(postDto);
        }

        [Authorize(Roles = "Insan Resurslari")]
        [HttpPut]
        public async Task<IActionResult> Edit(RolePutDto putDto)
        {
            return await _roleService.Edit(putDto);
        }

        [Authorize(Roles = "Insan Resurslari, Menecer")]
        [HttpGet("all")]
        public async Task<IActionResult> GetAll(int page = 1)
        {
            return await _roleService.GetAll(page);
        }

        [Authorize(Roles = "Insan Resurslari, Menecer")]
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            return await _roleService.Get(id);
        }

        [Authorize(Roles = "Insan Resurslari")]
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            return await _roleService.Delete(id);
        }
    }
}
