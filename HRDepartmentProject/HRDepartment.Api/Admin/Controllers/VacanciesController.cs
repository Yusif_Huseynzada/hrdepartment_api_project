﻿using AutoMapper;
using HRDepartment.Api.Admin.Dtos.EmployeeDtos;
using HRDepartment.Api.Admin.Dtos;
using HRDepartment.Api.Helpers;
using HRDepartment.Core.Entities;
using HRDepartment.Core.Repositories;
using Microsoft.AspNetCore.Mvc;
using HRDepartment.Api.Admin.Dtos.VacancyDtos;
using HRDepartment.Api.Admin.Services.Interfaces;
using HRDepartment.Api.Admin.Services.Implementations;
using Microsoft.AspNetCore.Mvc.RazorPages;
using System.Data;
using Microsoft.AspNetCore.Authorization;

namespace HRDepartment.Api.Admin.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class VacanciesController: ControllerBase
    {
        private readonly IVacancyService _vacancyService;
      

        public VacanciesController(IVacancyService vacancyService)
        {
            _vacancyService = vacancyService;
        }

        [Authorize(Roles = "Menecer")]
        [HttpPost("")]
        public async Task<IActionResult> Create(VacancyPostDto postDto)
        {
            return await _vacancyService.Create(postDto);
        }

        [Authorize(Roles = "Menecer")]
        [HttpPut]
        public async Task<IActionResult> Edit(VacancyPutDto putDto)
        {
            return await _vacancyService.Edit(putDto);
        }

        [Authorize(Roles = "Insan Resurslari, Menecer, Isci")]
        [HttpGet("all")]
        public async Task<IActionResult> GetAll(int page = 1)
        {
            return await _vacancyService.GetAll(page);
        }

        [Authorize(Roles = "Insan Resurslari, Menecer, Isci")]
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            return await _vacancyService.Get(id);
        }

        [Authorize(Roles = "Insan Resurslari, Menecer")]
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            return await _vacancyService.Delete(id);
        }
    }
}
