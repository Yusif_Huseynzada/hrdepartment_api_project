﻿using AutoMapper;
using HRDepartment.Api.Admin.Dtos.VacancyDtos;
using HRDepartment.Api.Admin.Dtos;
using HRDepartment.Core.Entities;
using HRDepartment.Core.Repositories;
using Microsoft.AspNetCore.Mvc;
using HRDepartment.Api.Admin.Dtos.AccountDtos;
using HRDepartment.Data.Repositories;
using Microsoft.EntityFrameworkCore;

using System.Text;
using FluentValidation.Results;
using System.Security.Cryptography;
using static HRDepartment.Api.Admin.Dtos.AccountDtos.LoginDto;
using HRDepartment.Api.Admin.Services.Interfaces;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Authorization;
using HRDepartment.Api.Services;

namespace HRDepartment.Api.Admin.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AccountsController: ControllerBase
    {
        private readonly IAppUserService _appUserService;
        private readonly IJwtService _jwtService;
        private readonly IConfiguration _configuration;

        public AccountsController(IAppUserService appUserService, IJwtService jwtService, IConfiguration configuration)
        {
            _appUserService = appUserService;
            _jwtService = jwtService;
            _configuration = configuration;
        }
      
        [HttpPost("register")]
        public async Task<IActionResult> Register(AppUserPostDto postDto)
        {
            return await _appUserService.RegisterUser(postDto);
        }


        [HttpPost("login")]
        public async Task<IActionResult> Login(LoginDto loginDto)
        {
            return await _appUserService.Login(loginDto);
        }

        [Authorize(Roles = "Insan Resurslari")]
        [HttpPut]
        public async Task<IActionResult> Edit(AppUserPutDto putDto)
        {
            return await _appUserService.Edit(putDto);
        }

        [Authorize(Roles = "Insan Resurslari")]
        [HttpGet("all")]
        public async Task<IActionResult> GetAll(int page = 1)
        {
            return await _appUserService.GetAll(page);
        }
        [Authorize(Roles = "Insan Resurslari")]
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            return await _appUserService.Get(id);

        }

        [Authorize(Roles = "Insan Resurslari")]
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            return await _appUserService.Delete(id);
        }

        [HttpPost("refresh-token")]
        public async Task<ActionResult<AppUserRefreshTokenDto>> RefreshToken()
        {
            var user = new AppUser();
            if (user == null)
            {
                return BadRequest("Invalid user or configuration not found.");
            }
            var result = await _appUserService.RefreshToken(user, _configuration);
            return result;
        }
    }
}
