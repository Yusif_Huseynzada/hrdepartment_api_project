﻿using HRDepartment.Core.Entities;
using HRDepartment.Core.Repositories;
using HRDepartment.Data.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRDepartment.Data.Repositories
{
    public class AppUserRepository: Repository<AppUser>, IAppUserRepository
    {
        private readonly HRDepartmentDbContext _context;

        public AppUserRepository(HRDepartmentDbContext context) : base(context)
        {
            _context = context;
        }

       
    }
}
