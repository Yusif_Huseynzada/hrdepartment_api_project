﻿using HRDepartment.Core.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRDepartment.Data.Configurations
{
    internal class VacancyConfiguration : IEntityTypeConfiguration<Vacancy>
    {
        public void Configure(EntityTypeBuilder<Vacancy> builder)
        {
            builder.Property(X => X.Name).IsRequired(true).HasMaxLength(30);
            builder.Property(X => X.WorkCommitment).IsRequired(true).HasMaxLength(500);
            builder.Property(X => X.Requirement).IsRequired(true).HasMaxLength(500);
            builder.Property(x => x.Salary).HasColumnType("decimal(18,2)");
            builder.Property(b => b.CreatedAt).HasColumnType("date");
            builder.Property(b => b.Deadline).HasColumnType("date");
            builder.HasOne(x => x.Category).WithMany(x => x.Vacancies).OnDelete(DeleteBehavior.NoAction);
        }
    }
}
