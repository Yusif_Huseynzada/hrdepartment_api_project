﻿using HRDepartment.Core.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRDepartment.Data.Configurations
{
    internal class EmployeeConfiguration : IEntityTypeConfiguration<Employee>
    {
        public void Configure(EntityTypeBuilder<Employee> builder)
        {
            builder.Property(x => x.FullName).IsRequired(true).HasMaxLength(50);
            builder.Property(x => x.Email).IsRequired(true).HasMaxLength(50);
            builder.Property(x => x.DateOfRecruitment).IsRequired(true).HasMaxLength(25);
            builder.Property(x => x.Age).HasColumnType("int");
            builder.Property(x => x.Salary).HasColumnType("decimal(18,2)");
            builder.Property(x => x.Image).HasMaxLength(100);
            builder.HasOne(x => x.Category).WithMany(x => x.Employees).OnDelete(DeleteBehavior.NoAction);
        }
    }
}
