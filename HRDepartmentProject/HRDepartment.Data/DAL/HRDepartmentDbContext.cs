﻿using HRDepartment.Core.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace HRDepartment.Data.DAL
{
    public class HRDepartmentDbContext: DbContext
    {
        public HRDepartmentDbContext(DbContextOptions<HRDepartmentDbContext> options): base(options) 
        { 
        
        }
        public DbSet<Category> Categories { get; set; }
        public DbSet<Vacancy> Vacancies { get; set; }
        public DbSet<Employee> Employees { get; set; }
        public DbSet<AppUser> AppUsers { get; set; }
        public DbSet<Role> Roles { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfigurationsFromAssembly(Assembly.GetExecutingAssembly());

            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<AppUser>()
                .HasOne(u => u.Role)
                .WithMany(r => r.AppUsers)
                .HasForeignKey(u => u.RoleId);

            modelBuilder.Entity<Category>().HasData(

                new Category { Id = 1, Name = "Informasiya Texnologiyalari" },
                new Category { Id = 2, Name = "Marketing" },
                new Category { Id = 3, Name = "Dizayn" }

                );

            modelBuilder.Entity<Role>().HasData(

                new Role { Id = 1, Name = "Isci" },
                new Role { Id = 2, Name = "Menecer" },
                new Role { Id = 3, Name = "Insan Resurslari" }

                );
        }

    }
}
