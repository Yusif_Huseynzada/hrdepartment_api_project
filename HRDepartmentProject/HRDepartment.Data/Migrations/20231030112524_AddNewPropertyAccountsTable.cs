﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace HRDepartment.Data.Migrations
{
    public partial class AddNewPropertyAccountsTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "FullName",
                table: "AppUsers",
                type: "nvarchar(20)",
                maxLength: 20,
                nullable: false,
                defaultValue: "");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "FullName",
                table: "AppUsers");
        }
    }
}
