﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace HRDepartment.Data.Migrations
{
    public partial class CreatedCategoryAndEmployeeAllCrudMethods : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "DateOfRecruitment",
                table: "Employees",
                type: "nvarchar(25)",
                maxLength: 25,
                nullable: false,
                defaultValue: "");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DateOfRecruitment",
                table: "Employees");
        }
    }
}
